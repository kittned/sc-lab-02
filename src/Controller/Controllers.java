package Controller;
import Model.BankAccount;
import Veiw.InvestmentFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controllers {
	private ActionListener listener ;
	private BankAccount bank ;
	private InvestmentFrame show ;
	
	
	public Controllers()
	{	bank = new BankAccount(1000);
		show = new InvestmentFrame();
		listener = new AddInterestListener();
		show.getButton().addActionListener(listener);
		show.getLabel().setText("balance: " + bank.getBalance());
		
	}
	
	public static void main(String[] args){
		   new Controllers();
	   }
	
	  class AddInterestListener implements ActionListener
      {
         public void actionPerformed(ActionEvent event)
         {
            double rate = Double.parseDouble(show.getField().getText());
            double interest = bank.getBalance() * rate / 100;
            bank.deposit(interest);
            show.getLabel().setText("balance: " + bank.getBalance());
         }            
      }
      
	public Object getField() {
		// TODO Auto-generated method stub
		return null;
	}

}
